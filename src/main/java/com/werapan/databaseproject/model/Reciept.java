/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import com.werapan.databaseproject.dao.CustomerDao;
import com.werapan.databaseproject.dao.UserDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class Reciept {

    private int id;
    private Date create_date;
    private float total;
    private float cash;
    private int total_qty;
    private int userId;
    private int customerId;
    private User user;
    private Customer customer;
    private ArrayList<RecieptDetail> recieptDetails = new ArrayList();

    public Reciept(int id, Date create_date, float total, float cash, int total_qty, int userId, int customerId) {
        this.id = id;
        this.create_date = create_date;
        this.total = total;
        this.cash = cash;
        this.total_qty = total_qty;
        this.userId = userId;
        this.customerId = customerId;
    }

    public Reciept(Date create_date, float total, float cash, int total_qty, int userId, int customerId) {
        this.id = -1;
        this.create_date = create_date;
        this.total = total;
        this.cash = cash;
        this.total_qty = total_qty;
        this.userId = userId;
        this.customerId = customerId;
    }

    public Reciept(float total, float cash, int total_qty, int userId, int customerId) {
        this.id = -1;
        this.create_date = null;
        this.total = total;
        this.cash = cash;
        this.total_qty = total_qty;
        this.userId = userId;
        this.customerId = customerId;
    }
      public Reciept( float cash,  int userId, int customerId) {
        this.id = -1;
        this.create_date = null;
        this.total = 0;
        this.cash = cash;
        this.total_qty = 0;
        this.userId = userId;
        this.customerId = customerId;
    }

    public Reciept() {
        this.id = -1;
        this.create_date = null;
        this.total = 0;
        this.cash = 0;
        this.total_qty = 0;
        this.userId = 0;
        this.customerId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getCash() {
        return cash;
    }

    public void setCash(float cash) {
        this.cash = cash;
    }

    public int getTotal_qty() {
        return total_qty;
    }

    public void setTotal_qty(int total_qty) {
        this.total_qty = total_qty;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        this.userId = user.getId();
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
        this.customerId = customer.getId();
    }

    public ArrayList<RecieptDetail> getRecieptDetails() {
        return recieptDetails;
    }

    public void setRecieptDetails(ArrayList recieptDetails) {
        this.recieptDetails = recieptDetails;
    }

    @Override
    public String toString() {
        return "Reciept{" + "id=" + id + ", create_date=" + create_date + ", total=" + total + ", cash=" + cash + ", total_qty=" + total_qty + ", userId=" + userId + ", customerId=" + customerId + ", user=" + user + ", customer=" + customer + ", recieptDetails=" + recieptDetails + '}';
    }
    
    
    
    public void addRecieptDetail(RecieptDetail recieptDetail){
        recieptDetails.add(recieptDetail);
        calulateTotal();
    }
    
     
    public void addRecieptDetail(Product product,int qty){
         RecieptDetail rd = new RecieptDetail(product.getId(), product.getName(),
                        product.getPrice(), qty, qty * product.getPrice(), -1);
        recieptDetails.add(rd);
        calulateTotal();
    }
    
      public void delRecieptDetail(RecieptDetail recieptDetail){
        recieptDetails.remove(recieptDetail);
        calulateTotal();
    }
    public void calulateTotal(){
        int totalQty = 0;
        float total = 0.0f;
        for(RecieptDetail rd : recieptDetails){
            total += rd.getTotalPrice();
            totalQty += rd.getQty();
        }
        this.total_qty = totalQty;
        this.total = total;
    }

    public static Reciept fromRS(ResultSet rs) {
        Reciept reciept = new Reciept();
        try {
            reciept.setId(rs.getInt("reciept_id"));
            reciept.setCreate_date(rs.getTimestamp("create_date"));
            reciept.setTotal(rs.getFloat("total"));
            reciept.setCash(rs.getFloat("cash"));
            reciept.setTotal_qty(rs.getInt("total_qty"));
            reciept.setUserId(rs.getInt("user_id"));
            reciept.setCustomerId(rs.getInt("customer_id"));
            //Propulation
            CustomerDao customerDao = new CustomerDao();
            UserDao userDao = new UserDao();
            Customer customer = customerDao.get(reciept.getCustomerId());
            User user = userDao.get(reciept.getUserId());
            reciept.setCustomer(customer);
            reciept.setUser(user);
        } catch (SQLException ex) {
            Logger.getLogger(Reciept.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return reciept;
    }
}
